<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\MySession;
use App\Transformers\ShowUserTransformer;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function signIn(LoginRequest $request)
    {
        $pass_from_request = $request->password;
        $email = $request->email;
        $userId = User::where('email', $email)->value('id');
        $password = User::where('email', $email)->value('password');
        $date = new DateTime();

        if (!Hash::check($pass_from_request, $password)) {
            return response('Password or Username is wrong', 403);
        }
        if (Cache::has($userId)) {
            return response()->json([
                'token' => Cache::get($userId),
            ]);
        }

        $token = sha1($email . $password . $date->getTimestamp());
        Cache::put($token, $userId, 10);
        Cache::put($userId, $token, 10);

        return response()->json([
            'token' => $token,
        ]);
    }

    public function signOut(MySession $mySession, Request $request)
    {
        $token = $request->header('Authorization');
        if (!$mySession->find($token)) {
            return response('Nope', 403);
        }
        $mySession->expire($token);
        return response()->json([
            'status' => 'succeeded',
        ]);

    }

    public function checkAuth(MySession $mySession, Request $request)
    {
        $token = $request->header('Authorization');
        $find = $mySession->find($token);
        if ($find)
            $mySession->prolong($token);

        return response()->json([
            'status' => $find,
            'userId' => $find ? Cache::get($token) : false,
        ]);
    }
}
