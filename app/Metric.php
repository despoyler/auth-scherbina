<?php

namespace App;

use App\Interfaces\AdvertInterface;
use App\Interfaces\MetricInterface;
use App\Transformers\MetricTransformer;
use Illuminate\Database\Eloquent\Model;

class Metric extends Model implements MetricInterface
{
    protected $fillable = [
        'ip_address', 'refer', 'user_agent', 'advert_id'
    ];

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }

    //
    public function getInfo(): array
    {
        return fractal()
            ->item($this)
            ->transformWith(new MetricTransformer())
            ->toArray();
    }

    public function getIpAddress(): string
    {
        return $this->ip_address;
    }

    public function getRefer()
    {
        return $this->refer;
    }

    public function getUserAgent()
    {
        return $this->user_agent;
    }

    public function setIpAddress(string $ipAddress)
    {
        $this->ip_address = $ipAddress;
    }

    public function setRefer(string $refer)
    {
        $this->refer = $refer;
    }

    public function setUserAgent(string $userAgent)
    {
        $this->user_agent = $userAgent;
    }

    public function setAdvert(AdvertInterface $advert)
    {
        $this->advert()->associate($advert);
    }

    public function getAdvert(): AdvertInterface
    {
        return $this->advert()->first();
    }
}
