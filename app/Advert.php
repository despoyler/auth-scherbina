<?php

namespace App;

use App\Interfaces\AdvertInterface;
use App\Interfaces\SubCategoryInterface;
use App\Interfaces\UserInterface;
use App\Transformers\AdvertTransformer;
use Illuminate\Database\Eloquent\Model;

class Advert extends Model implements AdvertInterface
{
    protected $fillable = [
        'tittle', 'description', 'image', 'price', 'city'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites')->withTimestamps();
    }

    public function metrics()
    {
        return $this->hasMany(Metric::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class);
    }


    //
    public function getInfo(): array
    {
        return fractal()
            ->item($this)
            ->parseIncludes(['comments'])
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function getMetrics()
    {
        return $this->metrics()->get();
    }

    public function getComments()
    {
        return $this->comments()->get();
    }

    public function getRootCategory()
    {
        return $this->subcategory()->first()->mainCategory()->first();
    }

    public function getCategory(): SubCategoryInterface
    {
        return $this->subcategory()->first();
    }

    public function getCountInFavorite(): int
    {
        return $this->favorites()->count();
    }

    public function getAuthor(): UserInterface
    {
        return $this->user()->first();
    }

    public function findByStr(string $phrase): AdvertInterface
    {
        // TODO: Implement findByStr() method.
    }

    public function setTittle(string $tittle)
    {
        $this->tittle = $tittle;
    }

    public function getTittle(): string
    {
        return $this->tittle;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setImage(string $href)
    {
        $this->image = $href;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function setUser(UserInterface $user)
    {
        $this->user()->associate($user);
    }

    public function setCategory(SubCategoryInterface $subCategory)
    {
        $this->subcategory()->associate($subCategory);
    }

    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    public function getStatus(): bool
    {
        return $this->status;
    }

    public function getCreateTime(){
        return $this->created_at->toDateTimeString();
    }

    public function getUpdateTime(){
        return ($this->updated_at == $this->created_at) ? 'false' : $this->updated_at->toDateTimeString();
    }

}
