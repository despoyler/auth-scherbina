<?php

namespace App\daemons;

require '/home/dmitry/PhpstormProjects/backend/authApi/vendor/autoload.php';
$app = require_once '/home/dmitry/PhpstormProjects/backend/authApi/bootstrap/app.php';

use App\Mail\PasswordChange;
use App\Mail\TestMail;
use App\Mail\Welcome;
use App\User;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use PhpAmqpLib\Connection\AMQPStreamConnection;


$kernel = $app->make(Kernel::class);

$response = $kernel->handle(
    $request = Request::capture()
);

$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();
$channel->queue_declare('mails', false, true, false, false);
echo " [*] Waiting for messages. To exit press CTRL+C\n";
$callback = function ($msg) {
    $msgArr = json_decode($msg->getBody(), true);
    switch ($msgArr['type']){
        case 'passwordChange':
            $mailtype=new PasswordChange($msgArr['params']['userName'], $msgArr['params']['passChangeLink']);
            break;
        case 'welcome':
            $mailtype=new Welcome($msgArr['params']['userName']);
            break;
    }
    Mail::to($msgArr['recipient'])->send($mailtype);
    echo " [x] Done\n";
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};
$channel->basic_qos(null, 1, null);
$channel->basic_consume('mails', '', false, false, false, false, $callback);
while (count($channel->callbacks)) {
    $channel->wait();
}
$channel->close();
$connection->close();