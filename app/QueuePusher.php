<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 28.07.18
 * Time: 20:58
 */

namespace App;


use App\Interfaces\IQueueMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class QueuePusher
{
    private $connection;

    /**
     * QueuePusher constructor.
     * @param $connection
     */
    /*public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
    }*/

    /**
     * @param IQueueMessage $message
     */
    public function publish(IQueueMessage $message)
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $this->connection->channel();
        $channel->queue_declare('mails', false, true, false, false);

        $msg = new AMQPMessage(
            json_encode($message),
            array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
        );
        $channel->basic_publish($msg, '', 'mails');
        $channel->close();
        $this->connection->close();
    }

    public function close()
    {
        //TODO
    }

}