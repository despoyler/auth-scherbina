<?php

namespace App\Transformers;

use App\Advert;
use League\Fractal\TransformerAbstract;

class AdvertTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['comments'];

    public function transform(Advert $advert){
        return [
            'tittle' => $advert->getTittle(),
            'description' => $advert->getDescription(),
            'image' => $advert->getImage(),
            'price' => $advert->getPrice(),
            'createdAt' => $advert->getCreateTime(),
            'updatedAt' => $advert->getUpdateTime(),
            'byUser' => $advert->getAuthor()->getEmail(),
        ];
    }

    public function includeComments(Advert $advert){
        return $this->collection($advert->comments, new CommentTransformer());
    }
}
