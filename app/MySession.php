<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 16:16
 */

namespace App;


use App\Interfaces\MySessionInterface;
use Illuminate\Support\Facades\Cache;
use App\Interfaces\UserInterface;

class MySession implements MySessionInterface
{
    public function find(string $token): bool
    {
        if(Cache::has($token))
            return true;
        return false;
    }

    public function prolong(string $token)
    {
        $value = Cache::get($token);
        Cache::forget($token);
        Cache::forget($value);
        Cache::put($token,$value,10);
        Cache::put($value,$token,10);

    }

    public function expire(string $token)
    {
        $value = Cache::get($token);
        Cache::forget($token);
        Cache::forget($value);
    }

    public function getUser(string $token): UserInterface
    {
        $user = User::where('id', Cache::get($token))->first();
        return $user;
    }

}