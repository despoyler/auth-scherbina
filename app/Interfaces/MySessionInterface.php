<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 16:16
 */

namespace App\Interfaces;


interface MySessionInterface
{
    public function find(string $token): bool;

    //public static function createSession();
    public function prolong(string $token);

    public function expire(string $token);

    public function getUser(string $token): UserInterface;
}