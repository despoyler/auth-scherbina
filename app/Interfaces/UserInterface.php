<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 20.07.18
 * Time: 14:08
 */

namespace App\Interfaces;


use App\Http\Requests\UpdateUserRequest;
use App\ProfileData;

interface UserInterface
{
    public function getInfo(): array;

    public function getAdverts(): array;

    public function getFavorites(): array;

    public function changePassword(string $newPassword);

    public function updateProfile(ProfileData $profileData);

    public function getFirstName(): string;

    public function setFirstName(string $fName);

    public function getLastName(): string;

    public function setLastName(string $lName);

    public function getEmail(): string;

    public function setEmail(string $email);

    public function getCity(): string;

    public function setCity(string $city);

    public function setPassword(string $password);
}