<?php

use App\Mail\TestMail;
use App\QueuePusher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\EmailMessage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/mail', function (QueuePusher $queuePusher, Request $request){
    $msg = new EmailMessage('auu706@gmail.com', 'welcome');
    $msg->setParams(['userName' => 'Dmitry']);
    //$queuePusher=app(QueuePusher::class);
    $queuePusher->publish($msg);
} ); // todo test route -  to Delete
Route::group(['prefix' => 'v1'], function () {

    Route::post('/register', 'RegisterController@register');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@getUserInfo');
        Route::put('/', 'UserController@updateUserInfo');
    });

    Route::group(['prefix' => 'passwordChange'], function () {
        Route::get('/', 'PasswordChangeController@initChanging');
        Route::put('/', 'PasswordChangeController@passChanging');
    });

    Route::group(['prefix' => 'login'], function () {
        Route::get('/', 'LoginController@checkAuth');
        Route::post('/', 'LoginController@signIn');
        Route::delete('/', 'LoginController@signOut');
    });
});

